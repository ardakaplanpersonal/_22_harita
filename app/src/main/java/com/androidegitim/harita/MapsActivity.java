package com.androidegitim.harita;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.androidegitim.androidegitimlibrary.helpers.DialogHelpers;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private ArrayList<MarkerOptions> markerOptions = new ArrayList<>();

    private final int PERMISSION_REQUEST_LOCATION = 1;

    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);

        createLocation();

        askPermission();
    }

    private void createLocation() {

        MarkerOptions sydney = new MarkerOptions().position(new LatLng(-34, 151))
                .title("Marker in Sydney")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));

        MarkerOptions turkey = new MarkerOptions().position(new LatLng(38.918819, 33.530273))
                .title("Marker in Turkey")
                .snippet("Burası benim ülkem")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_like));

        MarkerOptions france = new MarkerOptions().position(new LatLng(46.583406, 2.636719)).title("Marker in France");

        markerOptions.add(sydney);
        markerOptions.add(turkey);
        markerOptions.add(france);
    }

    public void askPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {

                    DialogHelpers.showDialog(MapsActivity.this, "Konum bulmak için gerekli!", "Konuma ulaşmak için bu izin gerekli.",
                                             "Tamam", new DialogInterface.OnClickListener() {

                                @TargetApi(Build.VERSION_CODES.M)
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);
                                }
                            }, "Hayır", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                    Toast.makeText(getApplicationContext(), "İzin vermediniz.", Toast.LENGTH_SHORT).show();
                                }
                            }, null, null);

                } else {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_LOCATION);
                }

            } else {

                startMap();
            }
        } else {

            startMap();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_LOCATION && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            startMap();
        }
    }

    private void startMap() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(final GoogleMap googleMap) {

        this.googleMap = googleMap;

        googleMap.getUiSettings().setZoomControlsEnabled(true);

        googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        googleMap.setMyLocationEnabled(true);

        for (MarkerOptions markerOptions : markerOptions) {

            googleMap.addMarker(markerOptions);
        }

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {

                Log.i("HARİTA", "latitude : " + latLng.latitude);
                Log.i("HARİTA", "longitude : " + latLng.longitude);

                googleMap.addMarker(new MarkerOptions().position(latLng)
                                            .title("TIKLANAN YER")
                                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));

            }
        });

    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onPause() {
        super.onPause();

        try {

            googleMap.setMyLocationEnabled(false);

        } catch (Exception e) {
            //zaten ilk açılışta lazım sadece
        }
    }
}
